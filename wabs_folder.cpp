#include <wabs_folder.h>

void prepare_folder(const std::string& Path)
{
                std::vector<std::string> folder ;
                folder = csplit(Path,"/") ;
                std::string walk ;

                walk = "/" + folder[0] ;

                mkdir(walk.c_str(),0700) ;

                if( chdir(walk.c_str()) != 0 )
                {
                	std::cerr << "failed to access " << walk << std::endl ;
                }

				errno = 0 ;
                for(auto it = folder.begin()+1; it != folder.end() ; it++ )
                {
                        mkdir( it->c_str(), 0700 ) ;
                        switch(errno)
                        {
							case EEXIST :	break ;
							
							case EACCES :	std::cerr << "No permission to create " << *it << std::endl ;
											break ;
							default :		break ;
						}
                        walk += "/" + (*it)  ;
                        if( chdir(walk.c_str()) != 0 )
                        {
                        	std::cerr << "faled to access " << walk << std::endl ;
                        }
                }

				errno = 0 ;
                mkdir(folder.end()->c_str(),0700) ;
                switch(errno)
                {
					case EEXIST :	break ;
					
					case EACCES :	std::cerr << "No permission to create " << *folder.end() << std::endl ;
									break ;
					default :		break ;
				}
                
                errno = 0 ;
                if( mkdir("pkg",0700) != 0 )
                {
						if(errno != EEXIST)
							std::cerr << "failed to make dir " << walk + "/pkg " << std::endl ;
						if(errno == EACCES)
							std::cerr << "No permission to create " << walk + "/pkg " << std::endl ;
                }
                errno = 0 ;
                if( mkdir("sources",0700) != 0 )
                {
					if(errno != EEXIST)
                        std::cerr << "failed to make dir " << walk + "/sources" << std::endl ;
                    if(errno == EACCES)
						std::cerr << "No permission to create " << walk + "/sources " << std::endl ;
                }
}
