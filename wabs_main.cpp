#include <iostream>
#include <cstdio>
#include <wabs_class.h>

int main(int argc, char **argv)
{

abs_wrapper *abs ;
abs = new abs_wrapper;

if( argc > 1 )
{
    switch( abs->abs_arg( std::string( argv[1] ) ) )
    {
        case 1: 	abs->help() ;
                        break ;
        case 2: 	abs->check_arg(argc) ; abs->search(argv[2],0) ;
                        break ;
        case 3: 	abs->check_arg(argc) ; abs->install(argc, argv) ;
                        break ;
        case 4: 	abs->check_arg(argc) ; abs->build(argc, argv) ;
                        break ;
        case 5: 	abs->check_arg(argc) ; abs->get_source(argc, argv) ;
                        break ;
        case 6: 	abs->update(argc,argv) ;
                        break ;
        case 7: 	abs->check_arg(argc) ; abs->depends(argv[2]) ;
                        break ;
        case 8: 	abs->check_arg(argc) ; abs->view_detail(argv[2]) ;
                        break ;
        case 9:		abs->check_arg(argc) ; abs->edit(argv[2]) ;
                        break ;
        case 10:	abs->update_nonverbose(argc,argv) ;
                        break ;
        case 11:	abs->check_arg(argc) ; abs->post_edit_install(argc,argv) ;
                        break ;
        case 12:	abs->check_arg(argc) ; abs->post_edit_build(argc,argv) ;
                        break ;
        case 13:	abs->check_arg(argc) ; abs->remove(argv[2]) ;
                        break ;
        case 14:	abs->db_upgrade() ;
                        break ;
        case 15:	abs->edit_db_upgrade() ;
                        break ;
        case 16:	abs->list_wabsdb() ;
                        break ;
        case 17:	abs->upgrade() ;
                        break ;
        case 18:	abs->check_arg(argc) ; abs->selective_install(argc,argv) ;
                        break ;
        case 19:	abs->check_arg(argc) ;
                        break ;
        case 20:	abs->check_arg(argc) ; abs->edit_wabsdb(argc,argv) ;
                        break ;
        case 21:	abs->check_arg(argc) ; abs->list_repo(argv[2]) ;
                        break ;
        case 22:	if(!abs->upgrade_pkg()) std::cout << "No packages updated..." << std::endl ;
                        break ;
        case 23:	abs->init();
                        break ;
        default: 	abs->help() ;
                        break ;
    }
}
else
    abs->help() ;

delete abs ;


return 0 ;
}

