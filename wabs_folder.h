#include <string>
#include <vector>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <my_func.h>
#include <cerrno>
#include <cstring>

#ifndef WABS_FOLDER
#define WABS_FOLDER

void prepare_folder(const std::string& Path) ;

#endif