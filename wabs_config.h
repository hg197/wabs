/*
Parse config and get Variables
*/
#ifndef WABS_CONF_H
#define WABS_CONF_H 1

#include <my_func.h>
#include <string>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <wabs_class.h>

int config_check(	std::string& MAKE_OPT,
					bool& MAKE_OPT_found,
					std::string& MAKEPKG_OPT,
					std::vector<std::string>& OPT,
					bool& OPT_found,
					std::string& Editor,
					std::string& Path,
					std::vector<std::string>& Repo,
					std::string& ABSPath,
					std::string& SHM_dir,
					bool& SHM_found,
					char * LOGFILE
				);

int config_read(	std::ifstream& rcfile,
					std::string& MAKE_OPT,
					bool& MAKE_OPT_found,
					std::string& MAKEPKG_OPT,
					std::vector<std::string>& OPT,
					bool& OPT_found,
					std::string& Editor,
					std::string& Path,
					std::vector<std::string>& Repo,
					std::string& ABSPath,
					std::string& SHM_dir,
					bool& SHM_found
				);


#endif
