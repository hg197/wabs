/*
Collection of fcuntions to get information out of given PKGBUILD
- dependencies
- short information of the program/script
- version and realease
*/

#ifndef WABS_PKG_H
#define WABS_PKG_H 1

#include <fstream>
#include <string>
#include <vector>
#include <my_func.h>


void pkg_depend(const std::string &pkgbuild) ;

void pkg_info(const std::string &pkgbuild) ;

void pkg_version(const std::string &pkgbuild) ;

std::string pkg_version_no_output(const std::string &pkgbuild) ;

void pkg_edit(	std::string Path,
		std::string &P_add,
		std::string &pkgbuild ,
		std::string &MAKE_OPT,
		std::vector<std::string> &OPT,
		bool &OPT_found
		) ;

#endif
