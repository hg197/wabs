#include <wabs_class.h>

void abs_wrapper::help()
{

	std::fstream abs_file ;
	std::string abs_line, ABS_VERSION ;

	abs_file.open("/usr/bin/abs",std::ios::binary|std::ios::in) ;

	while( abs_file and abs_file.good() )
	{
			getline(abs_file,abs_line) ;
			if ( abs_line.find("ABS_VERSION=") < abs_line.size() )
			{
				ABS_VERSION = abs_line.substr(abs_line.find_first_of("=")+2,abs_line.rfind("\"")-1) ;
				break ;
			}

	}
	std::cout << "wabs Version: " << WABS_VERSION << std::endl ;
	std::cout << "ABS  Version: " + ABS_VERSION.substr(0,ABS_VERSION.size()-1) + "\n" << std::string(42,'-') << std::endl ;
	std::cout << "syntax:\t" << "wabs " << "<option> <repository/package>" << std::endl ;
	std::cout << "\t-b  build package" << std::endl ;
	std::cout << "\t-be edit before building" << std::endl ;
	std::cout << "\t-d  show dependencies of package" << std::endl ;
	std::cout << "\t-e  edit PKGBUILD of package" << std::endl ;
	std::cout << "\t-h  this help" << std::endl ;
	std::cout << "\t-i  build and install package" << std::endl ;
	std::cout << "\t-ie edit before building and installing" << std::endl ;
	std::cout << "\t-l  list wabs' package log" << std::endl ;
	std::cout << "\t-L  list content of repository" << std::endl ;
	std::cout << "\t-o  get source of a package" << std::endl ;
	std::cout << "\t-r  remove package" << std::endl ;
	std::cout << "\t-s  search package" << std::endl ;
	std::cout << "\t-u  update PKGBUILDs - verbose" << std::endl ;
	std::cout << "\t-uq update PKGBUILDs - silent" << std::endl ;
	std::cout << "\t-U  rebuild / reinstall wabs database" << std::endl ;
	std::cout << "\t-Ue edit PKGBUILDs before rebuilding / reinstalling wabs database" << std::endl ;
	std::cout << "\t-v  view details of package" << std::endl ;
	std::cout << "\t--update  update packages of wabsdb" << std::endl ;
	std::cout << std::endl ;
	std::cout << "wabs-related:"<< std::endl ;
	std::cout << "\t--upgrade upgrade wabs" << std::endl ;
	std::cout << "\t--remove remove package(s) from wabsdb list" << std::endl ;

	ABS_VERSION.clear() ;
	abs_line.clear() ;
	abs_file.close() ;
}
