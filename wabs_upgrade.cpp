#include <wabs_class.h>

void abs_wrapper::upgrade()
{

FILE * pkgbuild = fopen("/tmp/.PKGBUILD_wabs","w") ;


fprintf(pkgbuild,"# Maintainer: linux-ka <hg197 at gmx dot de>\n") ;
fprintf(pkgbuild,"pkgname=wabs\n");
fprintf(pkgbuild,"pkgver=0\n");
fprintf(pkgbuild,"pkgrel=1\n");
fprintf(pkgbuild,"pkgdesc=\"ABS wrapper written in C/C++\"\n");
fprintf(pkgbuild,"arch=(any)\n");
fprintf(pkgbuild,"license=(\'GPL\')\n");
fprintf(pkgbuild,"groups=()\n");
fprintf(pkgbuild,"depends=(\'abs\' \'pacman\')\n");
fprintf(pkgbuild,"makedepends=(\'git\')\n");
fprintf(pkgbuild,"provides=(wabs)\n");
fprintf(pkgbuild,"conflicts=()\n");
fprintf(pkgbuild,"replaces=()\n");
fprintf(pkgbuild,"backup=(etc/wabsrc)\n");
fprintf(pkgbuild,"options=(\'strip\' \'makeflags\' \'buildflags\')\n");
fprintf(pkgbuild,"install=\n");
fprintf(pkgbuild,"source=()\n");
fprintf(pkgbuild,"noextract=()\n");
fprintf(pkgbuild,"md5sums=() #generate with 'makepkg -g'\n");

fprintf(pkgbuild,"_gitroot=https://hg197@bitbucket.org/hg197/wabs.git\n");
fprintf(pkgbuild,"_gitname=wabs.git\n");

fprintf(pkgbuild,"build() {\n");
fprintf(pkgbuild,"  cd \"$srcdir\"\n");
fprintf(pkgbuild,"  msg \"Connecting to GIT server....\"\n");
fprintf(pkgbuild,"\n");
fprintf(pkgbuild,"  if [[ -d \"$_gitname\" ]]; then\n");
fprintf(pkgbuild,"    cd \"$_gitname\" && git pull origin\n");
fprintf(pkgbuild,"    msg \"The local files are updated.\"\n");
fprintf(pkgbuild,"  else\n");
fprintf(pkgbuild,"    git clone \"$_gitroot\" \"$_gitname\"\n");
fprintf(pkgbuild,"  fi\n");

fprintf(pkgbuild,"  msg \"GIT checkout done or server timeout\"\n");
fprintf(pkgbuild,"  msg \"Starting build...\"\n");

fprintf(pkgbuild,"  rm -rf \"$srcdir/$_gitname-build\"\n");
fprintf(pkgbuild,"  git clone \"$srcdir/$_gitname\" \"$srcdir/$_gitname-build\"\n");
fprintf(pkgbuild,"  cd \"$srcdir/$_gitname-build\"\n");

fprintf(pkgbuild,"  make\n");
fprintf(pkgbuild,"}\n");

fprintf(pkgbuild,"package() {\n");
fprintf(pkgbuild,"  cd \"$srcdir/$_gitname-build\"\n");
fprintf(pkgbuild,"  make DESTDIR=\"$pkgdir/\" install\n");
fprintf(pkgbuild,"}\n");

fprintf(pkgbuild,"pkgver() {\n");
fprintf(pkgbuild,"  cd \"$srcdir/$_gitname-build\"\n");
fprintf(pkgbuild,"  printf \"r%%s.%%s\" \"$(git rev-list --count HEAD)\" \"$(git rev-parse --short HEAD)\"\n");
fprintf(pkgbuild,"}\n");

fclose(pkgbuild);
chdir("/tmp") ;
system("makepkg -o -p .PKGBUILD_wabs") ;
system("makepkg --needed --noconfirm --rmdeps -ifp .PKGBUILD_wabs") ;
rem(".PKGBUILD_wabs") ;

}

