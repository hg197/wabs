#include <my_func.h>

void rem(const std::string& f)
{

		if ( remove(f.c_str()) )
		{
			std::cerr << "Error: Could not delete " << f << std ::endl ;
		}

return;
}

int sys_command(const std::string& cmd, std::vector<std::string> &aim)
{

	int Max_Buffer = 1024 ;
	char buffer[Max_Buffer] ;
	std::vector<std::string> dir ;

	FILE *stream ;

	stream = popen(cmd.c_str(),"r") ;

	if(!stream)
	{
        	exit(1) ;
	}
	while(!feof(stream))
	{
        	if(fgets(buffer, Max_Buffer,stream) != NULL)
        	{
                	dir.push_back(buffer) ;
        	}
	}
	pclose(stream) ;

	aim = dir ;

	return 1 ;
}

/* list directory; faster than popen from sys_command.*/

int ls_dir(std::string dir, std::vector<std::string> &files)
{

	DIR *dp ;
	struct dirent *dirp ;
	if((dp = opendir(dir.c_str()))== NULL)
	{
		std::cout<<"Error("<< errno << ") opening "<< dir << std::endl ;
		return errno ;
	}
	while((dirp = readdir(dp)) != NULL)
	{
		files.push_back(std::string(dirp->d_name)) ;
	}
	closedir(dp) ;
	return 1 ;
}

/* split string at blank position <delim>*/

void string_split(const std::string str,std::vector<std::string> &aim)
{
	std::string p;
	std::istringstream iss(str);
	while(iss)
	{
	iss >> p;
	aim.push_back(p);
	}

}

std::vector<std::string> &string_xsplit(const std::string &s, char delim, std::vector<std::string> &elems)
{
	std::stringstream ss(s) ;
	std::string item ;
	while(std::getline(ss, item, delim))
	{
		elems.push_back(item) ;
	}
return elems ;
}


std::vector<std::string> string_xsplit(const std::string &s, char delim)
{
    std::vector<std::string> elems ;
    return string_xsplit(s, delim, elems) ;
}

std::vector<std::string> split(const std::string& s,const std::string& delim)
{
        std::vector<std::string> token ;
		token.reserve(20) ;

        size_t found = s.find_first_not_of(delim.c_str(),0) ;
        size_t delim_pos = s.find_first_of(delim.c_str(),0) ;

	if( delim_pos == 0)
	{
		delim_pos = s.find_first_of(delim.c_str(),1) ;
	}

        while (found != std::string::npos or delim_pos != std::string::npos)
        {
                token.push_back( s.substr(found,delim_pos-found) ) ;


                found = s.find_first_not_of(delim.c_str(),delim_pos) ;
                delim_pos = s.find_first_of(delim.c_str(),found) ;

        }

        return token ;
}

std::vector<std::string> csplit(const std::string & s, const char * delim)
{
	std::vector<std::string> token ;
	token.reserve(20) ;

	char * item = NULL ;
	char * dup = strdup(s.c_str()) ;
	item = strtok(dup,delim) ;

	while(item != NULL)
	{
		if(strlen(item) > 0)
			token.push_back(item) ;
		item = strtok(NULL,delim) ;
	}

	free(item) ;
	free(dup) ;

return token ;
}
