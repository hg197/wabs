#include <wabs_class.h>

void abs_wrapper::install(int argc, char **argv )
{

	std::string pkg ;

	for(int n = 2 ; n < argc ; ++n )
	{
		prepare_abs_folder() ;
		pkg = argv[n] ;

		search(pkg,1) ;

		if( argc > 2 )
		{
			std::cout << std::string(GREEN_on) << "\nBuilding and Installing " << std::string(GREEN_off) << std::string(RED_on) << "----> " + pkg + "\n" << std::string(RED_off) << std::endl ;
		}
		if(MAKE_OPT_found or OPT_found)
		{
			pkg_edit(Path,P_add,pkg,MAKE_OPT,OPT,OPT_found) ;
		}
		shm_link(pkg,Path + "/" + P_add) ;
		if( chdir( (Path + "/" + P_add + "/" + pkg).c_str() ) != 0 )
		{
			std::cerr << "Cannot enter " << Path + "/" + P_add + "/" + pkg << " : " << strerror(errno) << std::endl ;
		}
		system( ("/usr/bin/makepkg -csif " + MAKEPKG_OPT).c_str() ) ;

		wabsdb(pkg,"insert") ;
		updatelog(pkg.c_str());

		*STAT = false ;
		chdir("/tmp") ;
	}
}

void abs_wrapper::selective_install(int &argc,char **argv)
{
	std::string argv_s , PKG, REPO ;

	for(int i = 2; i != argc; ++i)
	{
			prepare_abs_folder() ;
			argv_s = argv[i] ;
			REPO = argv_s.substr( 0, argv_s.find("/") ) ;
			PKG = argv_s.substr(argv_s.find("/") + 1, argv_s.size() -1 ) ;

			if( argv_s.find("/") < argv_s.size() )
			{
				if( std::find(Repo.begin(),Repo.end(),REPO) != Repo.end() )
				{
					ls_dir( Path + "/" + REPO , Path_cont ) ;
					if (std::find(Path_cont.begin(),Path_cont.end(),PKG) != Path_cont.end() )
					{
						std::cout << std::string(GREEN_on) << "\nBuilding and Installing " << std::string(GREEN_off) << std::string(RED_on) << "----> " + PKG + "\n" << std::string(RED_off) << std::endl ;
						if(MAKE_OPT_found or OPT_found)
						{
							pkg_edit(Path,REPO,PKG,MAKE_OPT,OPT,OPT_found) ;
						}
						shm_link(PKG,Path + "/" + REPO) ;
						if( chdir( (Path + "/" + REPO + "/" + PKG).c_str() ) != 0 )
						{
							std::cerr << "Cannot enter " << Path + "/" + REPO + "/" + PKG << " : " << strerror(errno) << std::endl ;
						}
						system( ("/usr/bin/makepkg -csif " + MAKEPKG_OPT).c_str() ) ;
						wabsdb(PKG,"insert") ;
						updatelog(PKG.c_str());
					}
					else
					{
						std::cerr << "Package '" << PKG << "' not found. Check spelling." << std::endl ;
					}

				}
				else
				{
					std::cerr << "Repository '" << REPO << "' not found. Check spelling or download via wabs -u " << REPO << std::endl ;
				}
			}
			argv_s.clear() ;
			PKG.clear() ;
			REPO.clear() ;
	}
}

void abs_wrapper::post_edit_install(int argc, char **argv )
{
	std::string pkg ;

	for(int n = 2 ; n < argc ; ++n )
	{
		prepare_abs_folder() ;
		pkg = argv[n] ;
		search(pkg,1) ;
		if( argc > 2 )
		{
			std::cout << std::string(GREEN_on) << "\nBuilding and Installing " << std::string(GREEN_off) << std::string(RED_on) << "----> " + pkg + "\n" << std::string(RED_off) << std::endl ;
		}
		if(MAKE_OPT_found or OPT_found)
		{
			pkg_edit(Path,P_add,pkg,MAKE_OPT,OPT,OPT_found) ;
		}
		system((Editor + " " + Path + "/" + P_add + "/" + pkg + "/" + "PKGBUILD" ).c_str()) ;
		shm_link(pkg,Path + "/" + P_add) ;
		if( chdir( (Path + "/" + P_add + "/" + pkg).c_str() ) != 0 )
		{
			std::cerr << "Cannot enter " << Path + "/" + P_add + "/" + pkg << " : " << strerror(errno) << std::endl ;
		}
		system( ("/usr/bin/makepkg -csif --noconfirm " + MAKEPKG_OPT).c_str() ) ;
		wabsdb(pkg,"insert") ;
		updatelog(pkg.c_str()) ;
		*STAT = false ;
		chdir("/tmp") ;
	}
}
