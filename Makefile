include config.mk 

all: $(SRC) $(TARGET)

%.o: %.cpp
	@echo "${CC} $<"
	@$(CC) $(FLAGS) $(LIBS) -c $< 

$(TARGET): $(OBJ)
	@echo "+++linking to binary+++"
	@echo "$(CC) $@"
	@$(CC) $(FLAGS) $(LIBS) $(OBJ) -o $(TARGET)

clean: 
	rm -f $(TARGET) $(OBJ) $(DTARGET) $(DOBJ)

install: all
	@echo "Installing $(TARGET) to $(subst //,/,$(DESTDIR)$(BINDIR))"
	@install -Dm 755 $(TARGET) $(DESTDIR)$(BINDIR)/$(TARGET)
	@echo "Installing manpages"
	@install -Dm 644 $(MAN1) $(DESTDIR)$(MAN1DIR)/$(MAN1)
	@install -Dm 644 $(MAN5) $(DESTDIR)$(MAN5DIR)/$(MAN5)
	@install -Dm 644 $(MNTRC) $(DESTDIR)$(EPREFIX)/$(MNTRC)

uninstall:
	@echo "Removing executable from $(subst //,/,$(DESTDIR)$(BINDIR))"
	@$(RM) $(DESTDIR)$(BINDIR)/$(TARGET)
	@echo "Removing manpages"
	@$(RM) $(DESTDIR)$(MAN1DIR)/$(MAN1)
	@$(RM) $(DESTDIR)$(MAN5DIR)/$(MAN5)
	@$(RM) $(DESTDIR)$(EPREFIX)/$(MNTRC)
up:
	@git add -A .
	@git commit
	@git push

.PHONY: all install uninstall
.SILENT: clean
