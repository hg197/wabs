#include <wabs_class.h>
#include <cstdlib>
#include <cstdio>
#include <algorithm>


void abs_wrapper::wabsdb_init()
{

	std::string line ;
	fdb.open( (HOME + "/.config/wabs/wabsdb").c_str() , std::ios::in | std::ios::binary ) ;

	if( fdb.is_open() )
	{
		while( fdb.good() )
		{
			getline( fdb, line ) ;
			if ( line.size() > 1 )
			{
				db_item.name = csplit(line," ")[0];
				db_item.version = "" ;
				if(csplit(line," ").size() != 1)
				{
					db_item.version += csplit(line," ")[1] ;
				}
				db_item.pkg = db_item.name + " " + db_item.version ;

				PKG_VERSION_DB[db_item.name] = db_item.version ;

				DBI.push_back(db_item) ;
				DBII.push_back(db_item.pkg) ;
				//DB.push_back(db_item.name) ;
				DB_LIST.push_back(db_item.name) ;

				// DB soll abgelöst werden
				DB.push_back(line) ; // enthält name und version als string

			}
		}
		fdb.close() ;
	}
	else
	{
		fdb.open( (HOME + "/.wabsdb").c_str() , std::ios::out | std::ios::binary ) ;
		fdb.close() ;
	}
}

void abs_wrapper::wabsdb( const std::string& pkg, const std::string& mode )
{

	db_item_t item ;

	switch ( mode_F[mode] )
	{
		// ist pkg in wabsdb namentlich gelistet
		case 1  : item.name = pkg ;
			  item.version = _version(pkg) ;
			  item.pkg = item.name + " " + item.version ;

			  if ( std::find(DB_LIST.begin(),DB_LIST.end(),item.name) == DB_LIST.end() )
			  {
		  		  fdb.open( (HOME + "/.config/wabs/wabsdb").c_str() , std::ios::app | std::ios::out | std::ios::binary) ;
				  fdb << item.pkg << std::endl ;
				  fdb.close() ;
			  }
			  else //  wenn ja, aber mit anderer Version, dann
			  {
				  if( std::find(DBII.begin(),DBII.end(),item.pkg) == DBII.end() )
				  {
					  fdb.open( (HOME + "/.config/wabs/wabsdb").c_str() , std::ios::trunc | std::ios::out | std::ios::binary) ;
					  for(auto it = DBII.begin(); it != DBII.end(); it++)
					  {
						  if( csplit(*it," ")[0] == item.name )
						  {
							  DBII.erase(it) ;
							  break ;
						  }
					  }
					  for ( auto i = DBII.begin(); i != DBII.end(); i++)
					  {
							fdb << *i << std::endl ;
					  }
					  fdb << item.pkg << std::endl ;
					  fdb.close() ;
				  }
			  }
			  DBII.push_back(item.pkg);
			  DB_LIST.push_back(item.name);

			  break ;

		case 2  : if ( std::find(DB.begin(),DB.end(),pkg) != DB.end() )
			  {
				fdb.open( (HOME + "/.config/wabs/wabsdb").c_str() , std::ios::trunc | std::ios::out ) ;
				DB.erase( std::find(DB.begin(),DB.end(),pkg) ) ;
				for ( auto i = DB.begin(); i != DB.end(); i++)
				{
					fdb << *i << std::endl ;
				}
				fdb.close() ;
			  }
			  break ;

		default : break ;
	}
}

void abs_wrapper::edit_wabsdb( int argc,char **argv )
{

	db_item_t item ;

	//for (int *i = new int(2) ; *i != argc; (*i)++)
	while(argc-- != 2)
	{
		item.name = argv[argc] ;
		item.version = _version(argv[argc]) ;
		item.pkg = item.name + " " + item.version ;

		if ( std::find(DB_LIST.begin(),DB_LIST.end(),argv[argc]) != DB_LIST.end() )
		{
			fdb.open( (HOME + "/.config/wabs/wabsdb").c_str() , std::ios::trunc | std::ios::out | std::ios::binary) ;
			for(auto it = DBII.begin(); it != DBII.end(); it++)
			{
				if( csplit(*it," ")[0] == item.name )
				{
					DBII.erase(it) ;
					break ;
				}
			}
			//DB.erase( std::find(DB.begin(),DB.end(),argv[argc]) ) ;
		    for ( auto it = DBII.begin(); it != DBII.end(); it++)
			{
				fdb << *it << std::endl ;
			}
			fdb.close() ;
		}
		else
		{
			std::cerr << "'"<< argv[argc] << "' is not listed in wabsdb." << std::endl ;
		}
	}
}

void abs_wrapper::list_wabsdb()
{
	if ( DB_LIST.size() != 1  )
	{
	std::cout << "wabs package log contains " + std::string (RED_on) << DB_LIST.size() << std::string(RED_off) + " entries:" << std::endl ;
	}
	else
	{
	std::cout << "wabs package log contains " + std::string (RED_on) << DB_LIST.size() << std::string(RED_off) + " entry:" << std::endl ;
	}
	std::cout << "-------" << std::endl ;

	for ( auto db = DB_LIST.begin(); db != DB_LIST.end(); db++ )
	{
		std::cout << std::setw(20) << std::left << *db ;

		if( DB_LIST.size() > 5 )
		{
			if( *db != DB_LIST.at(DB_LIST.size() -1 ) )
			{
				std::cout << std::setw(20) << std::left << csplit(*++db," ")[0] ;
			}
		}
		if ( DB_LIST.size() > 10 )
		{
			if( *db != DB_LIST.at(DB_LIST.size() - 1 ) )
			{
				std::cout << std::setw(20) << std::left << csplit(*++db," ")[0] ;
			}

		}
		if ( DB_LIST.size() > 15 )
		{
			if( *db != DB_LIST.at(DB_LIST.size() - 1 ) )
			{
				std::cout << std::setw(20) << std::left << csplit(*++db," ")[0] ;
			}
		}
		if ( DB_LIST.size() > 20 )
		{
			if( *db != DB_LIST.at(DB_LIST.size() - 1 ) )
			{
				std::cout << std::setw(20) << std::left << csplit(*++db," ")[0] ;
			}
		}
		std::cout << std::endl ;
	}
}