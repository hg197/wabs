#include <wabs_class.h>


abs_wrapper::abs_wrapper()
:
MAKE_OPT_found(false) ,
OPT_found(false) ,
STAT(new short)
{

Flag["-h"]  = 1 ;
Flag["-s"]  = 2 ;
Flag["-i"]  = 3 ;
Flag["-b"]  = 4 ;
Flag["-o"]  = 5 ;
Flag["-u"]  = 6 ;
Flag["-d"]  = 7 ;
Flag["-v"]  = 8 ;
Flag["-e"]  = 9 ;
Flag["-qu"] = 10 ;
Flag["-uq"] = 10 ;
Flag["-ei"] = 11 ;
Flag["-ie"] = 11 ;
Flag["-be"] = 12 ;
Flag["-eb"] = 12 ;
Flag["-r"]  = 13 ;
Flag["-U"]  = 14 ;
Flag["-Ue"]  = 15 ;
Flag["-eU"]  = 15 ;
Flag["-l"]   = 16 ;
Flag["--upgrade"] = 17 ;
Flag["-ii"] = 18 ;
Flag["-iie"] =19 ;
Flag["-eii"] = 19 ;
Flag["--remove"] = 20 ;
Flag["-L"] = 21 ;
Flag["--update"] = 22 ;
Flag["--init"] = 23 ;

mode_F["insert"] = 1 ;
mode_F["remove"] = 2 ;

HOME = getenv("HOME") ;
sys_command("/usr/bin/pacman -Qq",Installed) ;

for(auto it = Installed.begin(); it != Installed.end(); it++)
{
	PKG_INSTALLED[ it->substr(0,it->find("\n")) ] = true ;
}


abs_config() ;

wabsdb_init() ;
}

abs_wrapper::~abs_wrapper()
{
	Repo.clear() ;
	Installed.clear() ;
	Path_cont.clear() ;
	if (fdb.is_open()) fdb.close() ;
}

void abs_wrapper::check_arg(int arg)
{
	if(arg < 3)
	{
		std::cerr << "Missing argument. See wabs -h." << std::endl ;
		exit(1) ;
	}
}


void abs_wrapper::edit(const std::string &pkg)
{
	search(pkg,1) ;
	system((Editor + " " + Path + "/" + P_add + "/" + pkg + "/" + "PKGBUILD" ).c_str()) ;
}


bool abs_wrapper::upgrade_pkg()
{

// do this when updating progs installed by wabs

        bool del = false ;

        for(auto it = DB_LIST.begin(); it != DB_LIST.end(); it++)
        {
                // check if package from wabsdb is still installed by pacman

                if( std::find(Installed.begin(), Installed.end(), *it + "\n") != Installed.end() )
                {
                        del = false ;
                }
                else
                {
                        del = true ;
                }

                if(del)
                {
                        std::cout << *it << " is not installed anymore. --> removing from wabsd!" << std::endl ;
                        wabsdb(*it,"remove") ; //remove from wabsdb, and PKG_VERSION_DB and DBI,DBII,DB_LIST
                        DB_LIST.erase(it)  ;
                        del = false ;
                }
                else
                {
                        del = true ;
                }

//              check if package from wabsdb is still in official repositories present
                search(*it,2) ;
                if( !*STAT and del )
                {
                        std::cerr << *it << " is not kept in official pacman repositories. --> removing from db!" << std::endl ;
//                      remove from wabsdb, and PKG_VERSION_DB and DBI,DBII,DB_LIST
			wabsdb(*it,"remove") ;
			DB_LIST.erase(it)  ;
                }
        }

	std::vector<std::string> INSTALL_LIST ;
	std::string yes_no = "no" ;

	for(auto it = DB_LIST.begin(); it != DB_LIST.end(); it++)
	{
		if ( PKG_VERSION_DB[*it] != _version(*it) )
		{
			std::cout << std::setw(30) << std::left << *it + "(" + PKG_VERSION_DB[*it] + ")" << std::setw(10) << std::left << "----->" << _version(*it) << std::endl ;
			INSTALL_LIST.push_back(*it) ;
		}
	}

	if( INSTALL_LIST.size() != 0 )
	{
		std::cout << "Upgrade [y]es or [n]o...: " ;
		std::cin >> yes_no ;
	}

	if( yes_no != "y" and yes_no != "yes" and yes_no != "Y" and yes_no != "Yes" )
	{
		return false ;
	}

	if( INSTALL_LIST.size() != 0 )
	{
		for(auto it = INSTALL_LIST.begin() ; it != INSTALL_LIST.end() ; it++)
		{
			search(*it,1) ;
			std::cout << std::string(GREEN_on) << "\nBuilding and Installing " << std::string(GREEN_off) << std::string(RED_on) << "----> " + *it + "\n" << std::string(RED_off) << std::endl ;
			if(MAKE_OPT_found or OPT_found)
			{
				pkg_edit(Path,P_add,*it,MAKE_OPT,OPT,OPT_found) ;
			}
			prepare_abs_folder() ;
			shm_link(*it,Path + "/" + P_add) ;
			chdir( (Path + "/" + P_add + "/" + *it).c_str() ) ;
			system( ("/usr/bin/makepkg -csif " + MAKEPKG_OPT).c_str() ) ;

			wabsdb(*it,"insert") ;
			updatelog(it->c_str());
			*STAT = false ;
			chdir("/tmp") ;
		}
	}

return true ;

}


void abs_wrapper::abs_config() /* config file auslesen */
{
	config_check(MAKE_OPT, MAKE_OPT_found, MAKEPKG_OPT, OPT, OPT_found, Editor, Path, Repo, ABSPath, SHM_dir, SHM_found,LOGFILE) ;
}

void abs_wrapper::shm_link(const std::string &pkg, const std::string &src)
{

	if(SHM_found)
	{
		std::string SHM_path, cmd ;
		SHM_path = SHM_dir + "/" + getenv("USER") ;
		mkdir(SHM_path.c_str(),0777) ;
		chmod(SHM_path.c_str(),0700) ;
		SHM_path = SHM_path + "/" + "makepkg" ;
		mkdir(SHM_path.c_str(),0777) ;
		SHM_path = SHM_path + "/" + pkg ;
		mkdir(SHM_path.c_str(),0777) ;
		if( chdir( (SHM_path + " " + src + "/" + pkg + "/" + "src").c_str() ) != 0 )
		{
			symlink( SHM_path.c_str() ,(src + "/" + pkg + "/" + "src").c_str() ) ;
		}
	}
}

void abs_wrapper::depends(const std::string &pkg)
{
	search(pkg,1) ;
	std::string PF = Path + "/" + P_add + "/" + pkg + "/" + "PKGBUILD" ;
	pkg_depend(PF) ;
}

void abs_wrapper::list_repo(const std::string& arg)
{
		Path_cont.clear() ;
		ls_dir( Path + "/" + arg,Path_cont) ;
		short n = 0 ;

		for(auto it = Path_cont.begin(); it != Path_cont.end(); it++)
		{


			if( it->find(".") > it->size() and it->find("..") > it->size() )
			{
					if ( PKG_INSTALLED.find(*it) != PKG_INSTALLED.end() )
					{
						std::cout << std::setw(48) << std::left << *it + std::string(RED_on) + "[i]" + std::string(RED_off) ;
					}
					else
					{
						std::cout << std::setw(35) << std::left << *it ;
					}
				n++ ;
			}
			if( n == 3 )
			{
				std::cout << std::endl ;
				n = 0 ;
			}

		}
		if(Path_cont.size() % 3 != 2)
		{
			std::cout << std::endl ;
		}

}

void abs_wrapper::build(int argc, char **argv)
{
	std::string pkg ;

	for(int *n = new int(2) ; *n < argc ; (*n)++ )
	{
		pkg = argv[*n] ;
		search(pkg,1) ;
		if(MAKE_OPT_found or OPT_found)
		{
			pkg_edit(Path,P_add,pkg,MAKE_OPT,OPT,OPT_found) ;
		}
		if(argc > 2)
		{
			std::cout << std::string(GREEN_on) << "\nBuilding " << std::string(GREEN_off) << std::string(RED_on) << "----> " + pkg + "\n" << std::string(RED_off) << std::endl;
		}
		shm_link( pkg,Path + "/" + P_add ) ;
		if( chdir( (Path + "/" + P_add + "/" + pkg ).c_str() ) != 0 )
		{
			std::cerr << "Cannot enter " << Path + "/" + P_add + "/" + pkg << " : " << strerror(errno) << std::endl ;
		}
		system( ("/usr/bin/makepkg -csf " + MAKEPKG_OPT).c_str() ) ;
		*STAT = false ;
	}
}

void abs_wrapper::post_edit_build(int argc, char **argv)
{
	std::string pkg ;

	for(int *n = new int(2) ; *n < argc ; (*n)++ )
	{
		pkg = argv[*n] ;
		search(pkg,1) ;
		if(MAKE_OPT_found or OPT_found)
		{
			pkg_edit(Path,P_add,pkg,MAKE_OPT,OPT,OPT_found) ;
		}
		if(argc > 2)
		{
			std::cout << std::string(GREEN_on) << "\nBuilding " << std::string(GREEN_off) << std::string(RED_on) << "----> " + pkg + "\n" << std::string(RED_off) << std::endl;
		}
		system((Editor + " " + Path + "/" + P_add + "/" + pkg + "/" + "PKGBUILD" ).c_str()) ;
		shm_link( pkg,Path + "/" + P_add ) ;
		if( chdir( (Path + "/" + P_add + "/" + pkg ).c_str() ) != 0 )
		{
			std::cerr << "Cannot enter " << Path + "/" + P_add + "/" + pkg << " : " << strerror(errno) << std::endl ;
		}
		system( ("/usr/bin/makepkg -csf " + MAKEPKG_OPT).c_str() ) ;
		*STAT = false ;
	}
}

void abs_wrapper::get_source(int argc, char **argv)
{
	std::string pkg ;

	for(int *n = new int(2) ; *n < argc  ; (*n)++ )
	{
		pkg = argv[*n] ;
		search(pkg,1) ;
		if(argc > 2)
		{
			std::cout << std::string(GREEN_on) << "\nFetching source " << std::string(GREEN_off) << std::string(RED_on) << "----> " + pkg + "\n" << std::string(RED_off) << std::endl;
		}
		if( chdir(const_cast<char*>((Path + "/" + P_add + "/" + pkg ).c_str())) != 0 )
		{
			std::cerr << "Cannot enter " << Path + "/" + P_add + "/" + pkg << " : " << strerror(errno) << std::endl ;
		}
		system("/usr/bin/makepkg -o") ;
		pkg.clear() ;
		*STAT = false ;
	}
}

void abs_wrapper::db_upgrade()
{

	for (auto db = DB.begin() ; db != DB.end(); db++)
	{
		search(*db,1) ;
		std::cout << std::string(GREEN_on) << "\nBuilding and Installing " << std::string(GREEN_off) << std::string(RED_on) << "----> " + *db + "\n" << std::string(RED_off) << std::endl ;
		if(MAKE_OPT_found or OPT_found)
		{
			pkg_edit(Path,P_add,*db,MAKE_OPT,OPT,OPT_found) ;
		}
		shm_link(*db,Path + "/" + P_add) ;
		if( chdir( (Path + "/" + P_add + "/" + *db).c_str() ) != 0 )
		{
			std::cerr << "Cannot enter " << Path + "/" + P_add + "/" + *db << " : " << strerror(errno) << std::endl ;
		}
		system( ("/usr/bin/makepkg -csif " + MAKEPKG_OPT).c_str() ) ;
		*STAT = false ;
		chdir("/tmp") ;
	}

}

void abs_wrapper::edit_db_upgrade()
{

	for (auto db = DB.begin() ; db != DB.end(); db++)
	{
		search(*db,1) ;
		std::cout << std::string(GREEN_on) << "\nBuilding and Installing " << std::string(GREEN_off) << std::string(RED_on) << "----> " + *db + "\n" << std::string(RED_off) << std::endl ;
		if(MAKE_OPT_found or OPT_found)
		{
			pkg_edit(Path,P_add,*db,MAKE_OPT,OPT,OPT_found) ;
		}
		system((Editor + " " + Path + "/" + P_add + "/" + *db + "/" + "PKGBUILD" ).c_str()) ;
		shm_link(*db,Path + "/" + P_add) ;
		if( chdir( (Path + "/" + P_add + "/" + *db).c_str() ) != 0 )
		{
			std::cerr << "Cannot enter " << Path + "/" + P_add + "/" + *db << " : " << strerror(errno) << std::endl ;
		}
		system( ("/usr/bin/makepkg -csif " + MAKEPKG_OPT).c_str() ) ;
		*STAT = false ;
		chdir("/tmp") ;
	}

}



void abs_wrapper::remove(const std::string &pkg)
{
		wabsdb(pkg,"remove") ;
		system( ("sudo pacman -Rcns " + pkg).c_str() ) ;
}

void abs_wrapper::view_detail(const std::string& pkg)
{
	search(pkg,1) ;
	std::string PF = Path + "/" + P_add + "/" + pkg + "/" + "PKGBUILD" ;
	pkg_info(PF) ;
	pkg_version(PF) ;
}

std::string abs_wrapper::_version(const std::string& pkg)
{
	search(pkg,1) ;
	std::string PF = Path + "/" + P_add + "/" + pkg + "/" + "PKGBUILD" ;

	return pkg_version_no_output(PF) ;
}

int abs_wrapper::abs_arg(const std::string& flag)
{
// hier je nach flag integer zurückgeben für switch anweisung in abs_main.cpp
	try
	{
		return Flag[flag] ;
		throw -1 ;
	}
	catch(int& i)
	{
		return -1 ;
	}
}


void abs_wrapper::prepare_abs_folder()
{
		std::vector<std::string> folder ;
		folder = string_xsplit(Path,'/') ;
		std::string walk ;
		int e ;
		walk = "/"+folder[1] ;

		if( chdir(walk.c_str()) != 0 )
                {
                	std::cerr << "Cannot enter " + walk << " : " << strerror(errno) << std::endl ; 
                }

		for(auto it = folder.begin()+2; it != folder.end(); it++ )
		{
			try
			{
				walk += "/" + (*it)  ;
				e = mkdir( it->c_str(), 0700 ) ;

				if(e != 0)
				{
//					std::cerr << "Failed to create " << walk << " : " << strerror(errno) << std::endl ;
				}

				if(chdir(walk.c_str()) != 0)
				{
					throw e ;
				}
			}
			catch(const int& e)
			{
				std::cerr << "Failed to enter " << walk << " : " << strerror(errno) << std::endl ;
			}
		}

		if( mkdir("pkg",0700) != 0 )
		{
//			std::cerr << "failed to create " << walk + "/(pkg)"  << " : " << strerror(errno) << std::endl ;
		}
		if( mkdir("sources",0700) != 0 )
		{
//			std::cerr << "failed to create " << walk + "/(sources)" << " : " << strerror(errno) << std::endl ;
		}
}