#include <wabs_class.h>

void abs_wrapper::update(int argc, char **argv)
{
	std::string pkg ;

	if (argc > 2)
	{
		for (short i = 2 ; i < argc; i++)
		{
			if( std::find(Repo.begin(),Repo.end(), std::string(argv[i])) != Repo.end() )
			{
				pkg += std::string(argv[i]) ;
				pkg += " " ;
			}
			else
			{
				std::cerr << "Error : " + std::string(argv[i]) + " cannot be found in REPOCAT " << std::endl ;
			}
		}
		if( pkg.size() )
		{
			system( ("abs " + pkg).c_str() ) ;
		}

	}
	else
	{
		nice(20) ;
		pkg += "abs " ;
		for(auto it = Repo.begin(); it != Repo.end(); it++)
		{
			pkg += *it + " " ;
		}
		system(pkg.c_str()) ;
	}
}

void abs_wrapper::update_nonverbose(int argc, char **argv)
{
	std::string pkg ;

	if (argc > 2)
	{
		for (short i = 2 ; i < argc; i++)
		{
			if( std::find(Repo.begin(),Repo.end(), std::string(argv[i])) != Repo.end() )
			{
				pkg += std::string(argv[i]) ;
				pkg += " " ;
			}
			else
			{
				std::cerr << "Error : " + std::string(argv[i]) + " cannot be found in REPOCAT " << std::endl ;
			}
		}

		if( pkg.size() )
		{
			system( ("abs " + pkg).c_str() ) ;
		}

	}
	else
	{
		nice(20) ;
		system("abs 1>/dev/null") ;
	}
}
