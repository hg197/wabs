#include <wabs_class.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <my_func.h>

#define MAKEPKGCONF "/etc/makepkg.conf"

#define BACKUP "/etc/makepkg.conf.save"

void abs_wrapper::init()
{

	std::fstream File ;

	std::vector<std::string> cont ;
	std::string line ;
	std::string yes_no = "yes" ;

	File.open(MAKEPKGCONF,std::ios::in) ;

	// read makepkg.conf

	while( File.good() )
	{
		getline(File,line) ;
		cont.push_back(line) ;
	}

	File.close() ;

	// Backup old makepkg.conf ?

	File.open(BACKUP,std::ios::in);

	if( File.good() )
	{
		std::cerr << "Backup " << MAKEPKGCONF << "? [yes/no] : "  ;
		std::cin >> yes_no ;
		File.close() ;
	}

	if( yes_no == "yes" )
	{
		File.open(MAKEPKGCONF,std::ios::out) ;

		for(auto it = cont.begin(); it != cont.end(); it++)
		{
			File << *it << std::endl ;
		}

		File.close() ;
	}


	// process Path data

	auto parts = csplit(Path,"/") ;

	std::string p ;

	for(auto it = parts.begin(); it!=parts.end()-1;it++)
	{
		if(it->size() > 0)
		{
			p += "/" + *it ;
		}
	}

	std::cout << p << std::endl ;

	for(auto it = cont.begin(); it != cont.end(); it++)
	{
		if(it->size() > 10)
		{
			if( it->find("PKGDEST") == 0 )
			{
				std::cout << "Replacing PKGDEST by:\t\t" << p + "/pkg" << std::endl ;
				*it = "PKGDEST=" + p + "/pkg" ;
			}
			else if( it->find("SRCDEST") == 0 )
			{
				std::cout << "Replacing SRCDEST by:\t\t" << SHM_dir << std::endl ;
				*it = "SRCDEST=" + SHM_dir ;
			}
			else if( it->find("SRCPKGDEST") == 0 )
			{
				std::cout << "Replacing SRCPKGDEST by:\t" << p + "/src" << std::endl ;
				*it = "SRCPKGDEST=" + p + "/src" ;
			}
			else if( it->find("BUILDDIR") == 0 )
			{
				std::cout << "Replacing BUILDDIR by:\t\t" << SHM_dir << std::endl ;
				*it = "BUILDDIR=" + SHM_dir ;
			}
		}
	}


	File.open(MAKEPKGCONF,std::ios::out) ;

	for(auto it = cont.begin(); it != cont.end(); it++)
	{
		File << *it << std::endl ;
	}

	File.close() ;


return ;

}
