/*
 *  backbone of wabs
 *
 *
 */

#define WABS_VERSION "2.1.0"

#ifndef WABS_CLASS_H
#define WABS_CLASS_H 1

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdio>
#include <cerrno>
#include <cstring>
#include <ctime>
#include <unistd.h>
#include <map>
#include <sys/stat.h>
#include <sys/types.h>
#include <vector>
#include <string>
#include <my_func.h>
#include <colors.h>
#include <wabs_config.h>
#include <wabs_pkgbuild.h>
#include <wabs_folder.h>
#include <algorithm>

#include <unistd.h>

class abs_wrapper
{

public:
	abs_wrapper() ;
	~abs_wrapper() ;
	void updatelog(const char * prog) ;
	void check_arg(int arg) ;
	void search(const std::string& pkg,const int& intern) ;
	void install(int argc, char **argv) ;
	void post_edit_install(int argc, char **argv) ;
	void post_edit_build(int argc, char **argv) ;
	int abs_arg(const std::string& flag) ;
	void get_source(int argc, char **argv) ;
	void build(int argc, char **argv) ;
	void depends(const std::string& pkg) ;
	void view_detail(const std::string& pkg) ;
	void update(int argc, char **argv) ;
	void update_nonverbose(int argc, char **argv) ;
	void help() ;
	void edit(const std::string& pkg) ;
	void remove(const std::string& pkg) ;
	void db_upgrade() ;
	void edit_db_upgrade() ;
	void edit_wabsdb(int argc,char **argv) ;
	void list_wabsdb() ;
	void selective_install(int &argc,char ** argv) ;
	void list_repo(const std::string& arg) ;
	void upgrade() ;
	bool upgrade_pkg() ;
	void init() ;

private:
	void abs_config() ;
	void prepare_abs_folder() ;
	void shm_link(const std::string& pkg, const std::string& src) ;
	void is_installed(const std::string& pkg,std::vector<std::string>& ref) ;
	std::string _version(const std::string& pkg) ;
	struct db_item_t
	{
		std::string name ;
		std::string version ;
		std::string pkg ;
		bool operator==(const struct db_item_t &i)
		{
			return this->name == i.name && this->version == i.version && this->pkg == i.pkg ;
		}
	} ;
	db_item_t db_item ;
	bool SHM_found, MAKE_OPT_found, OPT_found ;
	std::string Path,Status,pkgbuild,MAKEPKG_OPT,MAKE_OPT,Editor,SHM_dir,Build_dir,Is_installed,P_add,ABSPath,HOME;
	char LOGFILE[128] ;

	void wabsdb(const std::string& pkg, const std::string& mode) ;
	void wabsdb_init() ;

	std::vector<std::string> Path_cont, Repo, DB, OPT , DB_LIST, DBII;
	std::vector<std::string>::iterator Repo_it ;
	std::vector<std::string> Installed ;
	std::vector<std::string>::iterator Inst_it ;
	std::vector<db_item_t> DBI ;

	std::map<std::string,int> Flag ;
	std::map<std::string,short> mode_F ;
	std::map<std::string,bool> PKG_INSTALLED ;
	std::map<std::string,std::string> PKG_VERSION_DB ; // <pkg,version>

	std::fstream fdb ;

	short *STAT ;
} ;

#endif
