#include <wabs_class.h>


void abs_wrapper::updatelog(const char * prog)
{
	errno = 0 ;
	time_t          timer ;
	struct tm *     tinfo ;

	char date[20] ;

	time(&timer) ;
	tinfo= localtime (&timer);

	strftime(date,sizeof(date),"<%F-%H:%M>",tinfo) ;


	FILE * LOG = fopen(LOGFILE,"a") ;
	if(errno)
	{
		fprintf(stderr,"Error - No logfile (%s): %s\n",LOGFILE,strerror(errno)) ;
		return ;
	}

	char * ver_1 = new char[strlen(PKG_VERSION_DB[prog].c_str())] ;
	char * ver_2 = new char[strlen(_version(prog).c_str())];

	sprintf(ver_1,"(%s)",PKG_VERSION_DB[prog].c_str()) ;
	sprintf(ver_2,"(%s)",_version(prog).c_str());

//	fprintf(LOG,"%-20s\t%-25s(%-8s)\t-->\t(%-1s)\n",date,prog,PKG_VERSION_DB[prog].c_str(),_version(prog).c_str()) ;
	fprintf(LOG,"%-20s\t%-25s%-8s\t-->\t%-1s\n",date,prog,ver_1,ver_2) ;

	fclose(LOG) ;


return ;
}