#include <wabs_config.h>
#include <wabs_pkgbuild.h>
#include <wabs_folder.h>

int config_check(	std::string& MAKE_OPT ,
			bool& MAKE_OPT_found ,
			std::string& MAKEPKG_OPT ,
			std::vector<std::string>& OPT ,
			bool& OPT_found,
			std::string& Editor,
			std::string& Path,
			std::vector<std::string>& Repo,
			std::string& ABSPath,
			std::string& SHM_dir,
			bool& SHM_found,
			char * LOGFILE
			)
{
	bool found = false ;
	std::string econf = "/etc/wabsrc" ;
	std::string hconf = getenv("HOME") ;
	sprintf(LOGFILE,"%s/.config/wabs/%s",hconf.c_str(),"wabslog") ;
	hconf += "/.config/wabs/wabsrc" ;

	std::ifstream conf_file ;
	conf_file.open(hconf.c_str()) ;

	if(conf_file.is_open()) // read $HOME/.wabsrc
	{
		config_read(conf_file,MAKE_OPT,MAKE_OPT_found,MAKEPKG_OPT,OPT,OPT_found,Editor,Path,Repo,ABSPath,SHM_dir,SHM_found) ;
		found = true ;
	}
	else
	{
		conf_file.close() ;
	}

	conf_file.open(econf.c_str()) ;

	if(conf_file.is_open() and !found) // read /etc/wabsrc
	{
		config_read(conf_file,MAKE_OPT,MAKE_OPT_found,MAKEPKG_OPT,OPT,OPT_found,Editor,Path,Repo,ABSPath,SHM_dir,SHM_found) ;
	}
	else
	{
		conf_file.close();
	}

return 0;
}

/*	*read config*  */

int config_read(	std::ifstream& rcfile ,
			std::string& MAKE_OPT ,
			bool& MAKE_OPT_found ,
			std::string& MAKEPKG_OPT,
			std::vector<std::string>& OPT,
			bool& OPT_found ,
			std::string& Editor,
			std::string& Path,
			std::vector<std::string>& Repo,
			std::string& ABSPath,
			std::string& SHM_dir,
			bool& SHM_found
			)
{
	std::string* Line = new std::string ;
	std::vector<std::string> *Line_vec = new std::vector<std::string> ;
	bool E_found = false ,P_found = false , R_found = false, ABS_found = false , MAKEPKG_OPT_found = false  ;


	while(!rcfile.eof())
	{
		getline(rcfile,*Line) ;
		if(!Line->find("EDITOR"))
		{
	//		string_split(*Line,*Line_vec) ;
			*Line_vec = csplit(*Line," \t,") ;
			for(ushort i = 2; i < Line_vec->size() ; ++i)
			{
				Editor += Line_vec->at(i) + " " ;
			}
			Line_vec->clear() ;
			E_found = true ;
		}
		else if(!Line->find("SHM"))
		{
//			string_split(*Line,(*Line_vec)) ;
			*Line_vec = csplit(*Line," \t,") ;
			SHM_dir = Line_vec->at(2) ;
			Line_vec->clear() ;
			SHM_found = true ;
		}
		else if(!Line->find("PATH"))
		{
//			string_split(*Line,*Line_vec) ;
			*Line_vec = csplit(*Line," \t,") ;
			Path = Line_vec->at(2) ;
			Line_vec->clear() ;
			P_found = true ;
		}
		else if(!Line->find("REPOCAT"))
		{
//			string_split(*Line,*Line_vec) ;
			*Line_vec = csplit(*Line," \t,") ;
			for(ushort i = 2; i < Line_vec->size(); ++i)
			{
				Repo.push_back(Line_vec->at(i)) ;
				R_found = true ;
			}
			Line_vec->clear() ;
		}
		else if(!Line->find("ABSPATH"))
		{
//			string_split(*Line,*Line_vec) ;
			*Line_vec = csplit(*Line," \t,") ;
			ABSPath = Line_vec->at(2) ;
			ABS_found = true ;
			std::ifstream ABSfile(ABSPath.c_str()) ;
			if(!ABSfile)
			{
				std::cout << "ABSPath not found." << std::endl ;
			}
			Line_vec->clear() ;
		}
		else if(!Line->find("MAKEPKG_OPT"))
		{
//			string_split(*Line,*Line_vec) ;
			*Line_vec = csplit(*Line," \t,") ;
			for(ushort i = 2 ; i < Line_vec->size() ; ++i)
			{
				MAKEPKG_OPT += Line_vec->at(i) + " " ;
			}
			Line_vec->clear() ;
			MAKEPKG_OPT_found = true ;
		}
		else if(!Line->find("OPTIONS"))
		{
//			string_split(*Line,*Line_vec) ;
			*Line_vec = csplit(*Line," \t,") ;
			for(ushort i = 2 ; i < Line_vec->size() ; ++i )
			{
				OPT.push_back( "'" + Line_vec->at(i) + "'" );
			}
			Line_vec->clear() ;
			OPT_found = true ;

		}
		else if(!Line->find("MAKE_OPT"))
		{
//			string_split(*Line,*Line_vec) ;
			*Line_vec = csplit(*Line," \t,") ;
			for(ushort i = 2 ; i < Line_vec->size() ; ++i)
			{
				MAKE_OPT += Line_vec->at(i) + " " ;
			}
			Line_vec->clear() ;
			MAKE_OPT_found = true ;
		}
		else
		{
			Line_vec->clear() ;
		}
	}


	if(!E_found){
		Editor = "vi" ;
	}
	if(!R_found){
		Repo.push_back("core") ;
		Repo.push_back("extra") ;
		Repo.push_back("community") ;
	}
	if(!P_found){
		std::cout << "Path to parrent directory for core/community/extra/... PKGBUILDs not set." << std::endl ;
		exit(1) ;
	}
	else
	{
		prepare_folder(Path) ;
	}
	if(!ABS_found){
		std::cout << "Path to ABS not set correctly!" << std::endl ;
	}
	if(!MAKEPKG_OPT_found)
	{
		MAKEPKG_OPT = " " ;
	}
	if(!OPT_found)
	{
		OPT.push_back(" ") ;
	}
	if(!MAKE_OPT_found)
	{
		MAKE_OPT = " " ;
	}

	rcfile.close() ;

delete Line ;
delete Line_vec ;

return 0 ;
}


