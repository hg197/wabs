#include <wabs_class.h>

/*vergleiche ob Paket in Installed-Array vorhanden ist*/
void abs_wrapper::is_installed(const std::string& pkg,std::vector<std::string>& ref)
{
	Is_installed = " " ;
	for(Inst_it = ref.begin() ; Inst_it < ref.end() ; Inst_it++)
	{
		if( *Inst_it == pkg + "\n" )
		{
			Is_installed = std::string(RED_on) + "[installed]" + std::string(RED_off) ;
			if ( std::find(DB_LIST.begin(),DB_LIST.end(), pkg) != DB_LIST.end() )
			{
				Is_installed.insert( Is_installed.find("]"), " via wabs" ) ;
			}
			break ;
		}
	}
}

/* suche Paket in Ordnerinhalt (repository) */
void abs_wrapper::search(const std::string& file,const int& intern)
{
	Path_cont.clear() ;
	P_add.clear() ;
	*STAT = false ;

	unsigned short i = 0 ;

	for(Repo_it = this->Repo.begin(); Repo_it < this->Repo.end(); Repo_it++)
	{
		ls_dir( Path + "/" + *Repo_it,Path_cont) ;
		for(auto it = Path_cont.begin(); it < Path_cont.end(); it++)
		{
			if( it->find(file) != std::string::npos and intern == 2)
			{
				if( *it == file )
				{
					*STAT = true ;
					return ;
				}
			}
			if( it->find(file) != std::string::npos and intern == 1)
			{
				if( *it == file )
				{
					P_add = *(Repo_it) ;
					*STAT = true ;
					break ;
				}
			}
			if( it->find(file) != std::string::npos and intern == 0 )
			{
				if( !(*STAT) )
				{
					std::cout << *Repo_it << ":\n" << std::string(25,'-') << std::endl ;
					*STAT = true ;
				}
				is_installed(*it,Installed) ;
				std::cout << std::setw(40) << std::left << *it << "\t" << Is_installed << std::endl ;
			}
		}
		Path_cont.clear() ;
		if( intern and *STAT )
		{
			P_add = *Repo_it ;
			break ;
		}
		if( *STAT and i < Repo.size() - 1 )
		{
			std::cout << std::endl ;
		}
		*STAT = false ;
		i++ ;
	}
	if(P_add.empty() and intern != 2 ) exit(1) ;
}

