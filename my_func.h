/*
 useful functions to interact with OS/Shell
*/

#ifndef MY_FUNC_H
#define MY_FUNC_H 1

#include <iostream>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <string>
#include <cstring>
#include <vector>
#include <cstdio>
#include <list>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>


/*nimm shell command <cmd> und füge stdscreen output in ziel vector<string> aim */
int sys_command(const std::string& cmd, std::vector<std::string> &aim) ;

int ls_dir(std::string dir, std::vector<std::string> &files) ;

void string_split(const std::string str,std::vector<std::string> &aim) ;

std::vector<std::string> &string_xsplit(const std::string &s, char delim, std::vector<std::string> &elems) ;

std::vector<std::string> string_xsplit(const std::string &s, char delim) ;

std::vector<std::string> split(const std::string& s,const std::string& delim) ;

std::vector<std::string> csplit(const std::string & s, const char * delim) ;

void rem(const std::string& f) ;


#endif
