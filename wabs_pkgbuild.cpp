#include <wabs_pkgbuild.h>
#include <cstring>
#include <iomanip>


void pkg_depend(const std::string &pkgbuild)
{
	std::fstream file ;
	std::string line ;
	std::string *Line = new std::string ;
	std::vector<std::string> *Lin_vec = new std::vector<std::string>,Lin_vec_c ;

	file.open(pkgbuild.c_str(),std::ios::in|std::ios::binary) ;

	while( !file.eof() )
	{
		getline(file,*Line);
		if( !Line->find("makedepends=") )
		{
			*Lin_vec = csplit(*Line,"=\'() \t") ;
			std::cout << Lin_vec->at(0) << ": " ;
			for( unsigned int i = 1 ; i < Lin_vec->size(); i++)
			{
				if( (Lin_vec->at(i).find(">") != std::string::npos) or (Lin_vec->at(i).find("<") != std::string::npos) ) 
							std::cout << Lin_vec->at(i) ;
				else
							std::cout << Lin_vec->at(i) << " " ;
			}
			std::cout << std::endl ;
		}
		else if(!Line->find("depends="))
		{
			*Lin_vec = csplit(*Line,"=\'()\t ") ;
			std::cout << "depends: " ;
			for(auto it = Lin_vec->begin() +1; it != Lin_vec->end(); it++)
			{
				//Lin_vec_c = string_xsplit((*Lin_vec).at(*i),'\'') ;
				if(it->find(">") != std::string::npos or it->find("<") != std::string::npos)
					std::cout << *it ;
				else
					std::cout << *it << " " ;
			}
			std::cout << std::endl ;
		}
		Lin_vec->clear() ;
	}
	Lin_vec->clear() ;

	delete Line ;
	delete Lin_vec ;
}

void pkg_info(const std::string &pkgbuild)
{
	std::fstream file ;
	std::string *Line = new std::string ;
	file.open( pkgbuild.c_str(), std::ios::in|std::ios::binary ) ;

	while(!file.eof())
	{
		getline(file,*Line) ;
		if(!Line->find("pkgdesc="))
		{
			std::cout << std::setw(8) << std::left << "Info: " << Line->substr(Line->find("=") + 1,Line->size()-1)  + "\n" ;
			break ;
		}
	}
	file.close() ;

	delete Line ;
}


void pkg_version(const std::string &pkgbuild)
{
	std::fstream file ;
	std::string *Line = new std::string ;
	file.open(pkgbuild.c_str()) ;

	while(file.good())
	{
		getline(file,*Line) ;
		if(!Line->find("pkgver="))
		{
			char buf[20] ;
			sscanf(Line->c_str(),"%s",buf) ;
			*Line = buf ;
			std::cout << "version: " << Line->substr(Line->find("=") + 1,Line->size()-1) + "\n" ;
		}
		if(!Line->find("pkgrel="))
		{
			char buf[20] ;
			sscanf(Line->c_str(),"%s",buf) ;
			*Line = buf ;
			std::cout << "release: " << Line->substr(Line->find("=") + 1,Line->size()-1) + "\n" ;
		}
	}
	file.close() ;
	delete Line ;
}

std::string pkg_version_no_output(const std::string &pkgbuild)
{
	std::fstream file ;
	std::string *Line = new std::string ;
	std::string version ;

	file.open(pkgbuild.c_str()) ;

	while(file.good())
	{
		getline(file,*Line) ;
		if(!Line->find("pkgver="))
		{
			char buf[20] ;
			sscanf(Line->c_str(),"%s",buf) ;
			*Line = buf ;
			version = Line->substr(Line->find("=") + 1,Line->size()-1)  ;
		}
		if(!Line->find("pkgrel="))
		{
			char buf[20] ;
			sscanf(Line->c_str(),"%s",buf) ;
			*Line = buf ;
			version += "-" + Line->substr(Line->find("=") + 1,Line->size()-1) ;
		}
	}
	file.close() ;

	delete Line ;

return version ;

}

void pkg_edit(	std::string Path,
		std::string &P_add,
		std::string &pkgbuild,
		std::string &MAKE_OPT,
		std::vector<std::string> &OPT,
		bool &OPT_found
		 )
{
	std::fstream f ;
	std::string s , buffer ;
	bool stop=false ;
	bool OPT_found_in = false ;
	std::vector<std::string> vs,xs,ms ;

	int *pos = new int(0) ;
	// add options array or edit the array
	if(OPT_found)
	{
		f.open( (Path + "/" + P_add + "/" + pkgbuild + "/PKGBUILD").c_str()  ) ;

		while( f.good() )
		{
				getline(f,s) ;

				if(s.size() > 0)
				{
					if ( s.find("options=(")  != std::string::npos )
					{
						OPT_found_in = true ;
						for( auto it = OPT.begin(); it != OPT.end(); it++ )
						{
							if( s.find( it->substr(2,it->size() - 2) ) > s.size() and s.find(*it) > s.size() )
							{
								s.insert(s.size()-1," " + *it + " " ) ;
							}
						}

						vs.push_back(s + "\n") ;
					}
					else
					{
						vs.push_back(s + "\n") ;
					}
				}
				else
				{
						;
				}

				s.clear() ;
		}

		f.close() ;

		if (!OPT_found_in)
		{
			for (auto it = vs.begin(); it != vs.end(); it++ )
			{
				if ( it->find("source=(") < std::string::npos )
				{
					it = vs.insert(it,"options=()\n") ;
					for(auto opt_it = OPT.begin(); opt_it != OPT.end(); opt_it++)
					{
						it->insert(it->rfind(")"),*opt_it+" ") ;
					}
					it->erase(it->size()-3,1);
					break ;
				}

			}

		}

		if( !stop )
		{
			f.open( (Path + "/" + P_add + "/" + pkgbuild + "/PKGBUILD").c_str() ) ;

			for(auto it = vs.begin() ; it != vs.end() ; it++ )
			{
				f.write( it->c_str(), it->size() ) ;
			}
			vs.clear() ;
			s.clear() ;
			f.close() ;
		}
	}

	// add make flags
	f.open( (Path + "/" + P_add + "/" + pkgbuild + "/PKGBUILD").c_str()  ) ;

	while( f.good() )
	{
        	getline(f,s) ;
        	if( s.size() > 0 )
        	{
				string_split(s,xs) ; // split wabsrc line
				string_split(MAKE_OPT,ms) ; // split MAKE_OPT
				for( auto xsi = xs.begin() ; xsi != xs.end() ; xsi++ )
				{
					if( xsi->find("make") != std::string::npos
					    and
					    xsi->find("makedepends") == std::string::npos
					    and
					    xsi->find("makeflags") == std::string::npos
					    and
					    xsi->find("makepkg.conf") == std::string::npos
					    and
					    xsi->find("makepkg") == std::string::npos
					    and
					    xsi->find("makebuild") == std::string::npos
					    and
					    xsi->find("automake") == std::string::npos
					    and
					    xsi->find("cmake") == std::string::npos
					    and
					    xsi->find("qmake") == std::string::npos
					    and
    					    xsi->find("pkgname=") == std::string::npos
					    and
    					    xsi->find("pkgdesc=") == std::string::npos
					    and
    					    xsi->find("options=") == std::string::npos
					    and
    					    xsi->find("url=") == std::string::npos
					    and
    					    xsi->find("source=") == std::string::npos
					    and
					    xsi->find("make-") == std::string::npos
					    and
					    xsi->find("GNUmake") == std::string::npos
					    and
					    xsi->find("imake") == std::string::npos )
					{
						for( auto si = s.begin() ; si != s.end() ; si++ )
						{
							buffer = *si ;
							buffer += *(++si) ;
							buffer += *(++si) ;
							buffer += *(++si) ;

							(*pos)++ ;

							if( "make" == buffer )
							{
								*pos = *pos + 3 ;
								buffer.clear() ;

								++si ;
								for (unsigned int i = 0 ; i < ms.at(0).size(); i++ )
								{
									buffer += *(++si) ;

								}
								if ( buffer == ms.at(0) )
								{
									stop = true ;
								}
								break ;
							}

							--si ;
							--si ;
							--si ;

							buffer.clear() ;

						}
                				s.insert(*pos," " + MAKE_OPT) ;
						*pos = 0 ;
                				break ;
					} // end if
				} // end for
				vs.push_back(s + "\n") ;
			}
			else
			{
				vs.push_back(" ") ;
			}
			xs.clear() ;
        	s.clear() ;
	}

	f.close();
	if( !stop )
	{
		f.open( (Path + "/" + P_add + "/" + pkgbuild + "/PKGBUILD").c_str() ) ;

		for(auto it = vs.begin() ; it != vs.end() ; it++ )
		{
        		f.write( it->c_str(), it->size() ) ;
		}

		f.close() ;
	}
	delete pos ;
}
