TARGET = wabs
DTARGET = wabs-debug
MAN1 = wabs.1.gz
MAN5 = wabsrc.5.gz
MNTRC = wabsrc


SRC = $(wildcard *.cpp)
OBJ = $(SRC:.cpp=.o)

DOBJ = $(SRC:.cpp=.do)

CC = g++

FLAGS += -Wall -Wextra
FLAGS += -O3
FLAGS += -msse 
FLAGS += -msse2
FLAGS += -pipe
FLAGS += -march=native
FLAGS += -mtune=native
FLAGS += -std=c++11
FLAGS += -g
#FLAGS += $(shell pkg-config --cflags gtk+-2.0)

# directories

PREFIX = /usr
EPREFIX = /etc
BINDIR = $(PREFIX)/bin
MAN1DIR = /usr/share/man/man1
MAN5DIR = /usr/share/man/man5


#LIBS += $(shell pkg-config --libs gtk+-2.0)
LIBS += -I . # -l ncurses -l panel -l usb

